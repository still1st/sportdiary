﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeBehind="CreateTraining.aspx.cs"
    Inherits="SportDiary.Web.CreateTraining"
    MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <link href="Content/themes/base/jquery.ui.datepicker.css" rel="stylesheet"/>
    <script>
        function formatDate(date) {
            var dd = date.getDate();
            if (dd < 10) dd = '0' + dd;

            var mm = date.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;

            return date.getFullYear() + "." + mm + '.' + dd + " 00:00";
        }

        function Exercise(data) {
            this.Id = ko.observable(data.Id);
            this.Name = ko.observable(data.Name);
        }

        function ExerciseResult(data) {
            this.Id = 0;
            this.ExerciseId = ko.observable(data.ExerciseId);
            this.Weight = ko.observable(data.Weight);
            this.Repetitions = ko.observable(data.Repetitions);
            this.PartResultId = ko.observable(data.PartResultId);
        }

        function Part(data) {
            this.Id = ko.observable(data.Id);
            this.Name = ko.observable(data.Name);
        }

        function PartResult() {
            this.Id = 0;
            this.Time = ko.observable("00:00:00");
            this.TrainingPartId = ko.observable();
            this.ExerciseResults = ko.observableArray([]);
            this.CountOfRounds = ko.observable(0);
        };

        function Training(data) {
            this.Id = 0;
            this.Date = data.Date;
            this.TrainingPartResults = data.TrainingPartResults;
        }

        function TrainingViewModel() {
            var self = this;

            self.Date = ko.observable(formatDate(new Date()));
            self.PartResults = ko.observableArray([]);

            self.Parts = ko.observableArray([]);
            self.Exercises = ko.observableArray([]);

            self.addPartResult = function () {
                self.PartResults.push(new PartResult());
            };

            self.removePartResult = function (partResult) {
                self.PartResults.remove(partResult);
            };

            self.addExerciseResult = function (partResult) {
                partResult.ExerciseResults.push(new ExerciseResult({ Weight: 0, Repetitions: 0, PartResultId: partResult.Id }));
            };

            self.copyExerciseResult = function (exerciseResult) {
                $.each(self.PartResults(), function () {
                    if (this.ExerciseResults().indexOf(exerciseResult) > 0) {
                        this.ExerciseResults.push(new ExerciseResult({ Weight: exerciseResult.Weight(), Repetitions: exerciseResult.Repetitions(), PartResultId: exerciseResult.PartResultId, ExerciseId: exerciseResult.ExerciseId }));
                    }
                });
            }

            self.removeExerciseResult = function (exerciseResult) {
                $.each(self.PartResults(), function () {
                    this.ExerciseResults.remove(exerciseResult);
                });
            }

            self.saveTraining = function () {
                $.ajax({
                    type: "POST",
                    url: "/WebService.asmx/SaveTraining",
                    data: "{training: " + ko.toJSON(new Training({ Date: self.Date(), TrainingPartResults: self.PartResults })) + "}",
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        window.location = "/";
                    },
                    error: function (err) {
                        alert("Ошибка!" + err.status + " " + err.statusText);
                    }
                });
            };

            $.ajax({
                type: "POST",
                url: "/WebService.asmx/GetTrainingParts",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (allData) {
                    self.Parts = JSON.parse(allData.d);
                },
                failure: function (msg) {
                    alert(msg);
                }
            });

            $.ajax({
                type: "POST",
                url: "/WebService.asmx/GetExercises",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (allData) {
                    self.Exercises = JSON.parse(allData.d);
                },
                failure: function (msg) {
                    alert(msg);
                }
            });
        };

        $(document).ready(function () {
            var viewModel = new TrainingViewModel();
            ko.applyBindings(viewModel);
        });
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="panel panel-default">
        <div class="panel-heading form-inline">
            <label for="date">Дата:</label>
            <input id="date" data-bind="value: Date" class="form-control" />
        </div>

        <div data-bind="foreach: PartResults" class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading form-inline">
                    <div class="form-group">
                        <span>Тип: </span>
                        <select data-bind="options: $root.Parts, optionsText: 'Name', optionsValue: 'Id', value: TrainingPartId" class="form-control"></select>
                    </div>

                    <div class="form-group">
                        <span>Время:</span>
                        <input data-bind="value: Time" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                        <span>Раунды: </span>
                        <input data-bind="value: CountOfRounds" class="form-control"/>
                    </div>

                    <button data-bind="click: $root.removePartResult" class="btn btn-danger btn-sm glyphicon glyphicon-minus"></button>
                </div>

                <div class="panel-body">
                    <div data-bind="foreach: ExerciseResults, visible: ExerciseResults().length > 0" class="panel panel-info">
                        <div class="panel-heading form-inline">
                            <div class="form-group">
                                <span>Упражнение: </span>
                                <select data-bind="options: $root.Exercises, optionsText: 'Name', optionsValue: 'Id', value: ExerciseId" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <span>Вес: </span>
                                <input data-bind="value: Weight" class="form-control" />
                            </div>

                            <div class="form-group">
                                <span>Кол-во повторений</span>
                                <input data-bind="value: Repetitions" />
                            </div>

                            <button data-bind="click: $root.removeExerciseResult" class="btn btn-danger btn-sm glyphicon glyphicon-minus"></button>
                        </div>
                </div>
            </div>

            <div class="panel-footer">
                <button data-bind="click: $root.addExerciseResult" class="btn btn-success btn-sm glyphicon glyphicon-plus"></button>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <button data-bind="click: addPartResult" class="btn btn-success btn-sm glyphicon glyphicon-plus"></button>
    </div>
    </div>

    <div class="form-group">
        <button data-bind="click: saveTraining" class="btn btn-default">Сохранить</button>
    </div>
</asp:Content>
