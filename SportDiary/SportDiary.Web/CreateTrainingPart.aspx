﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateTrainingPart.aspx.cs"
    Inherits="SportDiary.Web.CreateTrainingPart"
    MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="form-inline">
        <asp:Label ID="Label1" runat="server" Text="Название:" AssociatedControlID="TxtName"></asp:Label>
        <asp:TextBox runat="server" ID="TxtName" CssClass="form-control"></asp:TextBox>

        <asp:Button ID="Button1" runat="server" Text="Добавить" OnClick="Add_Click" CssClass="btn btn-default" />
        
        <asp:Label ID="LblError" runat="server" CssClass="alert-danger"/>
    </div>

    <asp:GridView runat="server" ID="GridView" BackColor="White" AutoGenerateColumns="false" CssClass="table table-striped table-condensed">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Название" />
        </Columns>
    </asp:GridView>
</asp:Content>
