﻿using SportDiary.Model;
using System;
using System.Linq;
using SportDiary.Web.Models;

namespace SportDiary.Web
{
    public partial class CreateTrainingPart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new SportDiaryContext())
            {
                GridView.DataSource = db.TrainingParts.OrderBy(x => x.Name).ToList();
                GridView.DataBind();
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtName.Text))
            {
                LblError.Text = "Название части тренировки не заполнено";
                return;
            }

            var trimedName = TxtName.Text.Trim().ToLower();

            using (var db = new SportDiaryContext())
            {
                if (db.TrainingParts.Any(x => x.Name.ToLower() == trimedName))
                {
                    LblError.Text = string.Format("Часть тренировки с название '{0}' уже существует", TxtName.Text);
                    return;
                }

                db.TrainingParts.Add(new TrainingPart { Name = TxtName.Text });
                db.SaveChanges();

                Response.Redirect(Request.Url.AbsolutePath);
            }
        }
    }
}