﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true"
    CodeBehind="Default.aspx.cs"
    Inherits="SportDiary.Web._Default" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script>
        function TopResult(data) {
            this.Name = ko.observable(data.Name);
            this.Result = ko.observable(data.Result);
        }

        function MainViewModel() {
            var self = this;
            self.TopRepetitions = ko.observableArray([]);
            self.TopWeights = ko.observableArray([]);

            $.ajax({
                type: "POST",
                url: "/WebService.asmx/GetTopRepetitions",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (allData) {
                    var topResults = JSON.parse(allData.d);
                    $.each(topResults, function (index, item) {
                        self.TopRepetitions.push(new TopResult(item));
                    });

                },
                failure: function (msg) {
                    alert(msg);
                }
            });

            $.ajax({
                type: "POST",
                url: "/WebService.asmx/GetTopWeights",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (allData) {
                    var topResults = JSON.parse(allData.d);
                    $.each(topResults, function (index, item) {
                        self.TopWeights.push(new TopResult(item));
                    });

                },
                failure: function (msg) {
                    alert(msg);
                }
            });
        }

        $(document).ready(function () {
            ko.applyBindings(new MainViewModel());

            $('table table table').hide();
            $('table table').css('cursor', 'pointer');
            $('table table').click(function() {
                $(this).find('table').slideToggle();
            });
        });
    </script>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="container well">
            <div class="col-md-6">
                <div id="carousel-example-generic" class="carousel" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="Images/slide0.jpg" alt="crossfit">
                            <div class="carousel-caption">
                                <h3>Crossfit</h3>
                            </div>
                        </div>
                        <div class="item">
                            <img src="Images/slide1.jpg" alt="crossfit">
                            <div class="carousel-caption">
                                <h3>Котейка</h3>
                            </div>
                        </div>
                        <div class="item">
                            <img src="Images/slide2.jpg" alt="crossfit">
                            <div class="carousel-caption">
                                <h3>Мурзейка</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3>ТОП повторений</h3>
                    </div>
                    <div class="panel-body">
                        <div data-bind="foreach: TopRepetitions">
                            <div class="row">
                                <div class="col-md-9">
                                    <span data-bind="text: Name"></span>
                                </div>

                                <div class="col-md-3">
                                    <span data-bind="text: Result"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3>ТОП весов</h3>
                    </div>
                    <div class="panel-body">
                        <div data-bind="foreach: TopWeights">
                            <div class="row">
                                <div class="col-md-9">
                                    <span data-bind="text: Name"></span>
                                </div>

                                <div class="col-md-3">
                                    <span data-bind="text: Result"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" OnRowDataBound="GridView_OnRowDataBound"
        BackColor="white"
        CssClass="table table-bordered table-condensed">
        <Columns>
            <asp:BoundField DataField="Date" HeaderText="Дата" />

            <asp:TemplateField HeaderText="Дополнительно">
                <ItemTemplate>
                    <asp:GridView runat="server" ID="gvPartResults" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="gvPartResults_OnRowDataBound"
                        CssClass="table table-condensed table-hover">
                        <Columns>
                            <asp:BoundField DataField="TrainingPart.Name" HeaderText="Название" />
                            <asp:BoundField DataField="Time" HeaderText="Время" />
                            <asp:BoundField DataField="CountOfRounds" HeaderText="Раунды" />

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:GridView runat="server" ID="gvExerciseResults" AutoGenerateColumns="False" CssClass="table table-striped table-condensed">
                                        <Columns>
                                            <asp:BoundField DataField="Exercise.Name" HeaderText="Название" />
                                            <asp:BoundField DataField="Weight" HeaderText="Вес" />
                                            <asp:BoundField DataField="Repetitions" HeaderText="Кол-во повторений" />
                                        </Columns>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
