﻿USE SportDiaryDb
GO

SELECT t.Date,
	   tp.Name,
	   tpr.Time,
	   e.Name,
	   er.Weight,
	   er.Repetitions
FROM dbo.Trainings as t

JOIN dbo.TrainingPartResults as tpr
ON t.Id = tpr.TrainingId

JOIN dbo.TrainingParts as tp
ON tpr.TrainingPartId = tp.Id

JOIN dbo.ExerciseResults as er
ON tpr.Id = er.TrainingPartResultId

JOIN dbo.Exercises as e
ON er.ExerciseId = e.Id

ORDER BY t.Date, tp.Name, e.Name