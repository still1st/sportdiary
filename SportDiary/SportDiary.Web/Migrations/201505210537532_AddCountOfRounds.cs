namespace SportDiary.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCountOfRounds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrainingPartResults", "CountOfRounds", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            AddColumn("dbo.TrainingParts", "CountOfRounds", c => c.Int(nullable: false, defaultValue: 0));
        }
    }
}
