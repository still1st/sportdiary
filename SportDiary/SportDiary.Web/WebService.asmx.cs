﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using SportDiary.Model;
using SportDiary.Web.Models;
using SportDiary.Web.ViewModels;

namespace SportDiary.Web
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        [WebMethod]
        public string GetTopRepetitions()
        {
            using (var db = new SportDiaryContext())
            {
                var topResults = new List<TopResult>();

                var exercies = db.Exercises.ToList();
                foreach (var exercise in exercies)
                {
                    var exercieResults = db.ExerciseResults.Where(x => x.ExerciseId == exercise.Id && x.Repetitions > 0);

                    if(exercieResults == null || !exercieResults.Any())
                        continue;
                    
                    topResults.Add(new TopResult{Name = exercise.Name, Result = exercieResults.Max(x => x.Repetitions)});
                }

                return new JavaScriptSerializer().Serialize(topResults.OrderByDescending(x => x.Result).ThenBy(x => x.Name));
            }
        }

        [WebMethod]
        public string GetTopWeights()
        {
            using (var db = new SportDiaryContext())
            {
                var topResults = new List<TopResult>();

                var exercies = db.Exercises.ToList();
                foreach (var exercise in exercies)
                {
                    var exercieResults = db.ExerciseResults.Where(x => x.ExerciseId == exercise.Id && x.Weight > 0);

                    if (exercieResults == null || !exercieResults.Any())
                        continue;

                    topResults.Add(new TopResult { Name = exercise.Name, Result = exercieResults.Max(x => x.Weight) });
                }

                return new JavaScriptSerializer().Serialize(topResults.OrderByDescending(x => x.Result).ThenBy(x => x.Name));
            }
        }

        [WebMethod]
        public bool SaveTraining(Training training)
        {
            if (training == null)
                return false;

            if (training.TrainingPartResults == null || !training.TrainingPartResults.Any())
                return false;

            using (var db = new SportDiaryContext())
            {
                db.Trainings.Add(training);
                db.SaveChanges();

                return true;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetTrainingParts()
        {
            using (var db = new SportDiaryContext())
            {
                var trainingParts = db.TrainingParts.OrderBy(x => x.Name).ToArray();

                return new JavaScriptSerializer().Serialize(trainingParts);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetExercises()
        {
            using (var db = new SportDiaryContext())
            {
                var exercises = db.Exercises.OrderBy(x => x.Name).ToArray();

                return new JavaScriptSerializer().Serialize(exercises);
            }
        }
    }
}
