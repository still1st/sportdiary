﻿using SportDiary.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportDiary.Model
{
    public class TrainingPart
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
