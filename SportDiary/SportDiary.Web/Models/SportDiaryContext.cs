﻿using System.Data.Entity;
using SportDiary.Model;

namespace SportDiary.Web.Models
{
    public class SportDiaryContext : DbContext
    {
        public DbSet<Training> Trainings { get; set; }

        public DbSet<TrainingPart> TrainingParts { get; set; }

        public DbSet<TrainingPartResult> TrainingPartResults { get; set; }

        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<ExerciseResult> ExerciseResults { get; set; }
    }
}
