﻿using SportDiary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportDiary.Web.Models
{
    public class ExerciseResult
    {
        public long Id { get; set; }

        public double Weight { get; set; }

        public int Repetitions { get; set; }

        public long ExerciseId { get; set; }

        public Exercise Exercise { get; set; }

        public long TrainingPartResultId { get; set; }

        public TrainingPartResult TrainingPartResult { get; set; }
    }
}