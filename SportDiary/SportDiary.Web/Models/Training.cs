﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportDiary.Model
{
    public class Training
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public ICollection<TrainingPartResult> TrainingPartResults { get; set; }
    }
}
