﻿using System;
using System.Collections.Generic;
using SportDiary.Web.Models;

namespace SportDiary.Model
{
    public class TrainingPartResult
    {
        public long Id { get; set; }

        public long TrainingId { get; set; }

        public long TrainingPartId { get; set; }

        public TimeSpan Time { get; set; }

        public int CountOfRounds { get; set; }

        public TrainingPart TrainingPart { get; set; }

        public Training Training { get; set; }

        public ICollection<ExerciseResult> ExerciseResults { get; set; } 
    }
}
