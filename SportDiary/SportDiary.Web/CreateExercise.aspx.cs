﻿using SportDiary.Model;
using System;
using System.Linq;
using SportDiary.Web.Models;

namespace SportDiary.Web
{
    public partial class CreateExercise : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new SportDiaryContext())
            {
                GridView.DataSource = db.Exercises.OrderBy(x => x.Name).ToList();
                GridView.DataBind();
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtName.Text))
            {
                LblError.Text = "Название упражнения не заполнено";
                return;
            }

            var trimedName = TxtName.Text.Trim().ToLower();

            using (var db = new SportDiaryContext())
            {
                if (db.Exercises.Any(x => x.Name.ToLower() == trimedName))
                {
                    LblError.Text = string.Format("Упражнение с название '{0}' уже существует", TxtName.Text);
                    return;
                }

                db.Exercises.Add(new Exercise { Name = TxtName.Text });
                db.SaveChanges();

                Response.Redirect(Request.Url.AbsolutePath);
            }
        }
    }
}