﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportDiary.Model
{
    public class SportDiaryContext : DbContext
    {
        public DbSet<Training> Trainings { get; set; }

        public DbSet<TrainingPart> TrainingParts { get; set; }

        public DbSet<TrainingPartResult> TrainingPartResults { get; set; }

        public DbSet<Exercise> Exercises { get; set; }
    }
}
