﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="SportDiary.Web.Statistics"
    MasterPageFile="Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="Scripts/d3/d3.js"></script>

    <style>
        .axis path, .axis line {
            fill: none;
            stroke: #333;
        }

        .axis .grid-line {
            stroke: #000;
            stroke-opacity: 0.2;
        }

        .axis text {
            font: 10px Verdana;
        }

        .bar {
            stroke: none;
            fill: #4682B4;
        }

            .bar:hover {
                fill: brown;
            }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <script>
        var data;
        $(document).ready(function () {
            var url = "WebService.asmx/GetTopRepetitions";

            d3.xhr(url)
                .header("Content-type", "application/json")
                .post(null, function (err, rawData) {
                    if (err) return alert(err);

                    var temp = JSON.parse(rawData.response);
                    data = JSON.parse(temp.d);

                    var height = 600,
                        width = 800,
                        margin = 30,
                        padding = 2;

                    var svg = d3.select("#chart")
                        .append("svg")
                        .attr("class", "axis")
                        .attr("width", width)
                        .attr("height", height);

                    var xAxisLength = width - 2 * margin;
                    var yAxisLength = height - 2 * margin;

                    var scaleX = d3.scale.ordinal()
                        .rangeRoundBands([0, width], .1)
                        .domain(data.map(function (d) { return d.Name; }));

                    var scaleY = d3.scale.linear()
                        .range([yAxisLength, 0])
                        .domain([0, d3.max(data, function (d) { return d.Result; })]);

                    var xAxis = d3.svg.axis()
                        .scale(scaleX)
                        .orient("bottom");

                    var yAxis = d3.svg.axis()
                        .scale(scaleY)
                        .orient("left");

                    // Отрисовка оси X
                    svg.append("g")
                        .attr("class", "x-axis")
                        .attr("transform", "translate(" + margin + "," + (height - margin) + ")")
                        .call(xAxis);

                    // Отрисовка оси Y
                    svg.append("g")
                        .attr("class", "y-axis")
                        .attr("transform", "translate(" + margin + "," + margin + ")")
                        .call(yAxis);

                    d3.selectAll("g.y-axis g.tick")
                        .append("line")
                        .classed("grid-line", true)
                        .attr("x1", 0)
                        .attr("y1", 0)
                        .attr("x2", xAxisLength)
                        .attr("y2", 0);

                    var g = svg.append("g")
                        .attr("class", "body")
                        .attr("transform", "translate(" + margin + ", 0)");

                    g.selectAll("rect.bar")
                        .data(data)
                        .enter()
                        .append("rect")
                        .attr("class", "bar");

                    g.selectAll("rect.bar")
                        .data(data)
                        .attr("x", function (d) {
                            return scaleX(d.Name);
                        })
                        .attr("y", function (d) {
                            return scaleY(d.Result) + margin;
                        })
                        .attr("height", function (d) {
                            return yAxisLength - scaleY(d.Result);
                        })
                        .attr("width", function (d) {
                            return Math.floor(xAxisLength / data.length) - padding;
                        });
                });
        });
    </script>

    <div id="chart"></div>
</asp:Content>
