﻿using System;

namespace SportDiary.Web.ViewModels
{
    [Serializable]
    public class TopResult
    {
        public string Name { get; set; }
        public double Result { get; set; }
    }
}