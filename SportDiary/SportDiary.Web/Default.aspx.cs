﻿using System.Data.Entity;
using SportDiary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SportDiary.Web.Models;

namespace SportDiary.Web
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var db = new SportDiaryContext())
            {
                var trainings = db.Trainings.OrderByDescending(x => x.Date).ToList();
                GridView.DataSource = trainings;
                GridView.DataBind();
            }
        }

        protected void GridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType != DataControlRowType.DataRow)
                return;

            var trainingId = (long)GridView.DataKeys[e.Row.RowIndex].Value;
            var gvPartResults = e.Row.FindControl("gvPartResults") as GridView;

            if(gvPartResults == null)
                return;

            using (var db = new SportDiaryContext())
            {
                var trainingPartResults = db.TrainingPartResults.Where(x => x.TrainingId == trainingId).Include("TrainingPart").ToList();
                gvPartResults.DataSource = trainingPartResults;
                gvPartResults.DataBind();
            }
        }

        protected void gvPartResults_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType != DataControlRowType.DataRow)
                return;

            var partResultId = (long) (sender as GridView).DataKeys[e.Row.RowIndex].Value;
            var gvExerciseResults = e.Row.FindControl("gvExerciseResults") as GridView;

            if(gvExerciseResults == null)
                return;

            using (var db = new SportDiaryContext())
            {
                gvExerciseResults.DataSource =
                    db.ExerciseResults.Where(x => x.TrainingPartResultId == partResultId).Include("Exercise").ToList();
                gvExerciseResults.DataBind();
            }

        }
    }
}